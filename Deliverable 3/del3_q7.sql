create OR REPLACE view genres as (select ID as TMP_ID from GCD_STORY_GENRE
where genre in ('humor','crime','romance'))

create OR REPLACE view division as (select STORY_ID,TMP_ID as GENRE_ID from GCD_STORY_TO_GENRE, genres
MINUS
SELECT * FROM GCD_STORY_TO_GENRE)

create OR REPLACE view fin as (select DISTINCT(STORY_ID) as str_id from GCD_STORY_TO_GENRE
    MINUS SELECT DISTINCT(STORY_ID) from division)

select Distinct (feature) from fin
inner join GCD_STORY on fin.str_id = GCD_STORY.ID
-- another
-- with fin as (select g1.STORY_ID from GCD_STORY_TO_GENRE g1,GCD_STORY_TO_GENRE g2,GCD_STORY_TO_GENRE g3
-- where g1.STORY_ID=g2.STORY_ID and g2.STORY_ID = g3.STORY_ID
-- and g1.GENRE_ID =2 and g2.GENRE_ID =7 and g3.GENRE_ID =13)
-- select feature from fin
-- inner join GCD_STORY on fin.STORY_ID = GCD_STORY.ID


