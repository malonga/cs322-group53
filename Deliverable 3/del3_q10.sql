select year, count(*) from (
                  SELECT extract(year from to_date(I1.PUBLICATION_DATE DEFAULT NULL ON CONVERSION ERROR, 'MON YYYY')) as year
                  FROM GCD_ISSUE I1
                  where extract(year from
                                to_date(I1.PUBLICATION_DATE DEFAULT NULL ON CONVERSION ERROR, 'MON YYYY')) is not null
              ) tmp
group by tmp.year
having tmp.year >=1965 and tmp.year<=1975
order by tmp.year asc

