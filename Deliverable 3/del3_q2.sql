select name,cnt from GCD_PUBLISHER
inner join
(select PUBLISHER_ID,count(ID) as cnt from GCD_SERIES
group by PUBLISHER_ID
having count(ID)>500) tmp
on tmp.publisher_ID = GCD_PUBLISHER.ID
order by cnt desc