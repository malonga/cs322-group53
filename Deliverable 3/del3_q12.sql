select STDDATA_COUNTRY.name,tmp.maximum,tmp.duration from STDDATA_COUNTRY
    inner join (
select country_id, max(YEAR_ENDED-YEAR_BEGAN) as maximum, AVG(YEAR_ENDED-YEAR_BEGAN) as duration from GCD_PUBLISHER
where YEAR_BEGAN>=1600 and YEAR_ENDED<2020
group by country_id) tmp
on STDDATA_COUNTRY.ID =tmp.COUNTRY_ID
order by maximum desc
fetch next 10 rows only

