-- HUMOR CRIME ROMANCE

CREATE OR REPLACE VIEW HUMOR_STORIES AS
SELECT STORY_ID
FROM GCD_STORY_TO_GENRE INNER JOIN GCD_STORY_GENRE
ON GENRE_ID = ID
WHERE GENRE = 'humor'

;

CREATE OR REPLACE VIEW CRIME_STORIES AS
SELECT STORY_ID
FROM GCD_STORY_TO_GENRE INNER JOIN GCD_STORY_GENRE
ON GENRE_ID = ID
WHERE GENRE = 'crime'

;

CREATE OR REPLACE VIEW ROMANCE_STORIES AS
SELECT STORY_ID
FROM GCD_STORY_TO_GENRE INNER JOIN GCD_STORY_GENRE
ON GENRE_ID = ID
WHERE GENRE = 'romance'

;

CREATE OR REPLACE VIEW ROUNDED_STORIES AS
SELECT *
FROM HUMOR_STORIES
INTERSECT
SELECT *
FROM CRIME_STORIES
INTERSECT
SELECT *
FROM ROMANCE_STORIES

;

SELECT DISTINCT(FEATURE)
FROM GCD_STORY INNER JOIN ROUNDED_STORIES
ON STORY_ID = ID
;
