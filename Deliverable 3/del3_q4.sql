with tmp as (SELECT PUBLISHER_ID, COUNT(GCD_INDICIA_PUBLISHER.ID) as cnt FROM GCD_INDICIA_PUBLISHER
    where COUNTRY_ID in (
        SELECT ID
        FROM STDDATA_COUNTRY
        where name = 'Belgium'
    )
group by PUBLISHER_ID
order by cnt DESC
FETCH NEXT 1 ROWS ONLY)

select name,tmp.cnt from GCD_BRAND_GROUP
    inner join tmp on GCD_BRAND_GROUP.PUBLISHER_ID = tmp.PUBLISHER_ID

