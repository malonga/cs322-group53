create or replace view tmp as
(
select COUNTRY_ID,PUBLISHER_ID,count(*) as cnt
from GCD_SERIES
where COUNTRY_ID in (
    select country_id
    from GCD_PUBLISHER
    group by country_id
    having count(*) >= 200
)
group by (COUNTRY_ID,PUBLISHER_ID)
    )
create or replace view fin as
(
select COUNTRY_ID,
       cnt,
       PUBLISHER_ID,
       ROW_NUMBER() OVER (partition by country_id ORDER BY cnt desc) as num
from tmp
    )
select STDDATA_COUNTRY.name, GCD_PUBLISHER.name from fin
inner join GCD_PUBLISHER
on GCD_PUBLISHER.ID = fin.PUBLISHER_ID
inner join STDDATA_COUNTRY
on STDDATA_COUNTRY.ID = fin.COUNTRY_ID
where num in (1,2)