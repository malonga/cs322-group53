------------------------- SERIES COUNT PER PUBLISHER AND PUBLISHER'S ID (P_ID, CNT) ------------------------
CREATE OR REPLACE VIEW TEMP3_2 AS
SELECT S.PUBLISHER_ID AS P_ID, COUNT(S.ID) AS CNT
FROM GCD_SERIES S
GROUP BY S.PUBLISHER_ID
HAVING COUNT(S.ID) > 500
ORDER BY COUNT(S.ID) DESC;


SELECT NAME, CNT
FROM TEMP3_2 INNER JOIN GCD_PUBLISHER ON ID = P_ID

;

-- jh --- query2
select name,tmp.cnt from GCD_PUBLISHER
    inner join(
    select PUBLISHER_ID, count(ID) as cnt
    from GCD_SERIES
    group by PUBLISHER_ID
) tmp
on GCD_PUBLISHER.id = tmp.PUBLISHER_ID
where tmp.cnt > 500
order by tmp.cnt desc