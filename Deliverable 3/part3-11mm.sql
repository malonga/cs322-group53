
SELECT TITLE, CNT
FROM GCD_STORY INNER JOIN
    (Select ORIGIN_ID AS S_ID, COUNT(DISTINCT TARGET_ID) AS CNT 
    FROM GCD_STORY_REPRINT
    WHERE ORIGIN_ID IS NOT NULL
    GROUP BY (ORIGIN_ID)
    HAVING COUNT(DISTINCT TARGET_ID) >= 30
    )
ON ID=S_ID
ORDER BY CNT DESC
;
-- i changed  >= 30 since it is at least
-- jh
select S.TITLE,tmp.cnt from GCD_STORY S
    inner join (select origin_id,count(id) as cnt from GCD_STORY_REPRINT
group by ORIGIN_ID) tmp
on S.id = tmp.ORIGIN_ID
where tmp.cnt >= 30
order by tmp.cnt desc

