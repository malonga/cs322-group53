------------------------------------------------------------ Kim's Proposal ------------------------------------------------------------

SELECT NAME FROM STDDATA_LANGUAGE
    WHERE ID IN (
        SELECT DISTINCT(LANGUAGE_ID)
        FROM GCD_SERIES
        WHERE COUNTRY_ID NOT IN (
            SELECT ID
            FROM STDDATA_COUNTRY
            WHERE NAME = 'Switzerland'
        )
          and PUBLISHER_ID IN
              (
                  SELECT ID
                  FROM GCD_PUBLISHER
                  WHERE COUNTRY_ID IN
                        (
                            SELECT ID
                            FROM STDDATA_COUNTRY
                            WHERE NAME = 'Switzerland'
                        )
              )
    )
    
;





------------------------------------------------------------ Melvin's Proposal -----------------------------------------------------------
------brute cross product approach ------


SELECT DISTINCT L.name
from GCD_PUBLISHER P, GCD_SERIES S, STDDATA_LANGUAGE L, STDDATA_COUNTRY C
where P.COUNTRY_ID = C.ID AND C.NAME = 'Switzerland' AND L.ID = S.LANGUAGE_ID AND S.PUBLISHER_ID = P.ID AND S.COUNTRY_ID != C.ID
;