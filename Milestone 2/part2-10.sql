--------------------------- Checks ---------------------------

-- Do both proposals have same results :  --- talked about it
/*
(select * from prop1 MINUS select * from prop2) 
UNION ALL
(select * from prop2 MINUS select * from prop1)
;

should be empty and not produce error of union incompatibility
*/

------------------------ Melvin's proposal ------------------------------------------

-- OPTION 1
CREATE OR REPLACE VIEW temp10 AS
Select P.id
from GCD_PUBLISHER P
MINUS
Select DISTINCT P.id
from GCD_PUBLISHER P, GCD_SERIES S
where P.id = S.PUBLISHER_ID AND P.COUNTRY_ID = S.COUNTRY_ID
;

Select Count(*)
from temp10
;
