-- CARTESIAN JOIN APPROACH
/*
SELECT DISTINCT P.ID, P.name
FROM GCD_PUBLISHER P,GCD_SERIES S, STDDATA_LANGUAGE L, GCD_SERIES_PUBLICATION_TYPE T
where P.id = S.PUBLISHER_ID AND S.LANGUAGE_ID = L.ID AND L.NAME = 'Italian' AND S.PUBLICATION_TYPE_ID = T.ID AND T.name = 'album'
ORDER BY P.ID DESC
;
*/




--INNNER JOIN VERSION 


SELECT DISTINCT P.ID, P.name
FROM GCD_PUBLISHER P INNER JOIN GCD_SERIES S 
ON P.id = S.PUBLISHER_ID
INNER JOIN STDDATA_LANGUAGE L
ON S.LANGUAGE_ID = L.ID
INNER JOIN GCD_SERIES_PUBLICATION_TYPE T
ON S.PUBLICATION_TYPE_ID = T.ID
WHERE T.name LIKE 'album' AND L.NAME LIKE 'Italian'
ORDER BY P.ID DESC
;


