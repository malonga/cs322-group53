SELECT NAME FROM STDDATA_COUNTRY
    WHERE ID IN (
        SELECT DISTINCT(COUNTRY_ID)
        FROM GCD_SERIES
        WHERE PUBLISHER_ID IN (
            SELECT DISTINCT(PUBLISHER_ID)
            FROM GCD_SERIES
            where COUNTRY_ID IN (SELECT ID
                                 FROM STDDATA_COUNTRY
                                 WHERE NAME = 'Switzerland')
        )
    ) and NAME != 'Switzerland'
