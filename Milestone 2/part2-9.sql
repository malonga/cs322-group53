select C.NAME, COUNT (DISTINCT P.id) as cnt
from STDDATA_COUNTRY C, GCD_PUBLISHER P, GCD_SERIES S
where C.ID = P.COUNTRY_ID AND S.PUBLISHER_ID = P.ID AND P.COUNTRY_ID <> S.COUNTRY_ID
GROUP BY (C.NAME)
ORDER BY cnt DESC