

--Create view "Brandname US_Ind_count" 
Create or replace view "Brandname US_Ind_count" as
Select B.ID As ID, Count(I.ID) as cnt
from GCD_BRAND_GROUP B INNER JOIN  GCD_PUBLISHER P
ON B.Publisher_ID = P.ID
INNER JOIN GCD_INDICIA_PUBLISHER I 
ON P.ID = I.PUBLISHER_ID 
INNER JOIN STDDATA_COUNTRY C
ON I.COUNTRY_ID = C.ID
WHERE C.name LIKE 'United States'
Group by (B.ID)
;


--- actual query
Select B.name
from "Brandname US_Ind_count" v, GCD_BRAND_GROUP B
where v.id = B.id AND v.cnt = (Select Max(cnt)
from "Brandname US_Ind_count" v)
;
