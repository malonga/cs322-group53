with tmp as (select PUBLISHER_ID,COUNT(DISTINCT(LANGUAGE_ID)) as cnt from GCD_SERIES
    where GCD_SERIES.PUBLISHER_ID in (
        Select ID
        from GCD_PUBLISHER
        ORDER BY YEAR_BEGAN ASC
        FETCH FIRST 10 ROWS ONLY
    )
group by PUBLISHER_ID)


select GCD_PUBLISHER.name,tmp.cnt from tmp
inner join GCD_PUBLISHER on tmp.PUBLISHER_ID = GCD_PUBLISHER.ID
