

SELECT NAME, CNT
FROM GCD_INDICIA_PUBLISHER INNER JOIN
    --temp3_5_singleissueseries_cnt_per_IP
    (SELECT INDICIA_PUBLISHER_ID AS IP_ID, COUNT(*) AS CNT
    FROM -- TEMP3_5_SINGLE_ISSUE_SERIES 
        (SELECT SERIES_ID, INDICIA_PUBLISHER_ID
        FROM GCD_ISSUE
        GROUP BY (SERIES_ID, INDICIA_PUBLISHER_ID)
        HAVING COUNT(ID) = 1
        )
    GROUP BY (INDICIA_PUBLISHER_ID)
    HAVING COUNT(*) >= 400
    ORDER BY COUNT(*) DESC)
ON IP_ID = ID
ORDER BY CNT DESC
;


